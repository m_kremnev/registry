password:
	docker run --entrypoint htpasswd registry:2.7.0 -Bbn ${LOGIN} ${PASSWORD} > htpasswd

deploy:
	ssh deploy@${HOST} -p ${PORT} 'rm -rf registry && mkdir registry'
	scp -P ${PORT} docker-compose.yml deploy@${HOST}:registry/docker-compose.yml
	scp -P ${PORT} -r production deploy@${HOST}:registry/production
	scp -P ${PORT} ${HTPASSWD_FILE} deploy@${HOST}:registry/htpasswd
	ssh deploy@${HOST} -p ${PORT} 'cd registry && echo "COMPOSE_PROJECT_NAME=registry" >> .env'
	ssh deploy@${HOST} -p ${PORT} 'cd registry && docker-compose down --remove-orphans'
	ssh deploy@${HOST} -p ${PORT} 'cd registry && docker-compose pull'
	ssh deploy@${HOST} -p ${PORT} 'cd registry && docker-compose up -d'